function check(v, array, max){
    for (let i = 0; i<4; i++){
	if (array[i] == v){
	    if (v<max-1)
		v+=1;
	    else
		v=0;
	    i=0;
	}
    }
    return v;
}

var id = 0;
var score = 0;
var timer = 3000;
var countdown = setInterval(lost, timer);
var startingTime = new Date();
var lostMode = false;

function newPrice(max){
    var random = Array(4); 
    for(let i = 0; i < 4; i++){
	v = check(Math.floor(Math.random()*max), random, max);
	random[i]=v;
    }
    return random;
} 

function getMin(values){
    min = values[0];
    id=0;
    for (var i= 1; i<4; i++)
	if (values[i]<min){
	    min = values[i]
	    id = i;
	}
}

function updatePrices(){
    let max=1000;
    var values = newPrice(max);
    getMin(values);
    document.getElementById('price0').innerHTML = values[0];
    document.getElementById('price1').innerHTML = values[1];
    document.getElementById('price2').innerHTML = values[2];
    document.getElementById('price3').innerHTML = values[3];
}

var images = ["images/dress/dress1.png", "images/dress/dress2.png", "images/dress/dress3.png","images/dress/dress4.png","images/dress/dress5.png"] 

function updateImages(){
    var values = newPrice(5);
    document.getElementById('img0').src = images[values[0]];
    document.getElementById('img1').src = images[values[1]];
    document.getElementById('img2').src = images[values[2]];
    document.getElementById('img3').src = images[values[3]];
}

function won()
{
    
    score++;
    document.getElementById('score').innerHTML = score;
    if (score % 5 === 0){
	if (timer > 500)
	    timer -= 200;
    }
    newTry();
    
}

function lost()
{
    lostMode=true;
    clearInterval(countdown);

    var newTime= new Date();
    var elapsedTime = newTime-startingTime;
    if (score <= 1)
	document.getElementById('t').innerHTML = elapsedTime/(1000);
    else
	document.getElementById('t').innerHTML = elapsedTime/(1000*(score-1));

    document.getElementById('n').innerHTML = score;
    document.getElementById('game').style.display = "none";
    document.getElementById('replay').style.display = "block";

}


function checkTrue(img){
    if (img == id)
	won();
    else
	lost();
}

function replay(){
    lostMode=false;
    startingTime = new Date();
    score = 0;
    timer = 3000;
    document.getElementById('score').innerHTML = score;
    countdown = setInterval(lost, timer);

    newTry();
    
}


function newTry(){
    document.getElementById('replay').style.display = "none";
    document.getElementById('game').style.display = "block";

    updatePrices();
    updateImages();
    clearInterval(countdown);

    countdown = setInterval(lost, timer);
	
}

function keyDetected(e) {
    if(!lostMode){
	var keyCode = e.keyCode;
	if(keyCode == 37) {
            checkTrue(0)
	}
	if(keyCode == 40) {
            checkTrue(1)
	}
	if(keyCode == 38) {
            checkTrue(2)
	}
	if(keyCode == 39) {
            checkTrue(3)
	}
    }
    else {
	var keyCode = event.keyCode;
	if(keyCode == 13) {
	    document.getElementById("replayButton").click();

	}
	if(keyCode == 32) {
	    document.getElementById("replayButton").click();
	}
    }
};


