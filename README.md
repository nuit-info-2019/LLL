HOW TO PLAY :

Lancer le index.html avec un navigateur. Lorsque le jeu se lance, 4 vêtements
apparaissent, le but est de sélectionner le moins cher, en accord avec le thème
de la nuit de l'info sur la précarité étudiante.

Pour cliquer sur les boutons (Start et Replay):
-> click de la souris, touche Entrée ou espace
Pour sélectionner un vêtement :
-> click de la souris ou touche directionnelle indiquée en dessous.

À chaque bonne réponse, 4 nouveaux vêtements sont affichés. Il y a un timer pour
limiter le temps de réflexion du joueur. Au départ, le temps pour sélectionner
le vêtement est de 3s. Tous les 5 points de score, ce temps diminue de 0.2s (le
minimum étant fixé à 0.5s).

Lorsqu'on perd, un écran de replay apparaît affichant le score et la moyenne du
temps pris pour sélectionner une image.

Le jeu est également hébergé sur la page :
    lucie.mathe.emi.u-bordeaux.fr/night/error/

Tout a été codé from scratch, sans aucune base en HTML avant le début de la
soirée.
